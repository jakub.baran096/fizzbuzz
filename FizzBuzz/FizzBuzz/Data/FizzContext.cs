﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FizzBuzz.Models;
namespace FizzBuzz.Data
{
    public class FizzContext : DbContext
    {
        public FizzContext(DbContextOptions<FizzContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Log>().ToTable("Logs");
        }
    }
}
