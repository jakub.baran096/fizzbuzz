﻿using FizzBuzz.Data;
using FizzBuzz.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Newtonsoft.Json;
using System.Text.Json;
namespace FizzBuzz.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly FizzContext _context;
        private IHttpContextAccessor _accessor;
        private List<(int, string)> lista;
        private readonly ILogger<HomeController> _log;

        StringBuilder result;
        public HomeController(ILogger<HomeController> logger, FizzContext context, IHttpContextAccessor accessor, ILogger<HomeController> log)
        {
            _logger = logger;
            _context = context;
            _accessor = accessor;
            _log = log;
            lista = new List<(int, string)>() {
                (3,"fizz"),
                (5,"buzz"),
                (7,"wizz")
           };

            result = new StringBuilder("");
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(FizzBuzzModel Model)
        {
            if (Model.Number < 1 || Model.Number > 100)
            {
                ViewData["Odpowiedz"] = "Błędne dane";
                return View();
            }

            foreach (var current in lista)
                if (Model.Number % current.Item1 == 0) result.Append(current.Item2);
            ViewData["Odpowiedz"] = result.ToString() == "" ? Model.Number.ToString() : result.ToString();
            DoLogs();

            return View();
        }

        public void DoLogs()
        {
            var log = new Log()
            {
                Date = DateTime.Now,
                LocalAddress = _accessor.HttpContext.Connection.LocalIpAddress.ToString(),
                RemoteAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString()
            };
            _context.Logs.Add(log);
            _context.SaveChanges();
            _log.LogInformation(log.Date + " " + log.LocalAddress + " " + log.RemoteAddress);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
