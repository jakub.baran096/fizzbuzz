﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.Models
{
    public class Log
    {
        public int ID { get; set; }        
        public DateTime Date { get; set; }
        public string LocalAddress { get; set; }
        public string RemoteAddress { get; set; }

    }
}
